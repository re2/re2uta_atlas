# re2uta_atlas

This is a public release of the software used by the re2uta team in the DARPA VRC. Developed in the context of ros-fuerte. 
Compatability with any other ros release is unknown.


#### TO INSTALL:

This assumes you have DRC related packages already installed. If you don't then go here: `http://gazebosim.org/wiki/DRC/Install` and install `ROS groovy`

Dependencies:

    sudo apt-get install ros-groovy-bullet
    sudo apt-get install clang
    sudo apt-get install ros-groovy-interactive-markers
    sudo apt-get install libunittest++-dev
    sudo apt-get install ros-groovy-humanoid-nav-msgs
    sudo apt-get install ros-groovy-navigation
    sudo apt-get install ros-groovy-sbpl


Once complete:

    . /opt/ros/groovy/setup.bash
    cd catkin_ws
    cd src
    rosws update
    cd ..
    catkin_make

    cd ../rosbuild_ws
    rosinstall .
    source setup.bash
    rosws update

#### TO BUILD:    
    
    rosmake re2uta_atlasCommander

#### BUILD DRC SIM

Clone DRC sim into the `catkin_ws/src` directory:

    hg clone ssh://hg@bitbucket.org/re2/drcsim

To Build:

    cd drc
    cmake .
    make

#### TO DEMO:

##### DEMO BALANCING:

From 3 separate terminals, source the setup in each
    . setup.bash
    
 * Then in #1:

        roslaunch drcsim_gazebo atlas_sandia_hands.launch
    
 * Then in #2:
 
        roslaunch drcsim_gazebo keyboard_teleop.launch
        r
        q
        roslaunch re2uta_atlasCommander atlasCommander_nnCapture.launch  
    
That will start an instance of rviz with interactive markers.
    
 * Once it's up and running in #3 command the robot to balance on it's right foot:

        rostopic pub /commander/walkto re2uta_atlasCommander/WalkPlan \
        "header:
          seq: 0
          stamp:
            secs: 0
            nsecs: 0
          frame_id: ''
        start_swing_foot: 0
        goal_poses:
        - position:
            x: 0.0
            y: 0.0
            z: 0.0
          orientation:
            x: 0.0
            y: 0.0
            z: 0.0
            w: 1.0"

Now selecting "interact and dragging the robot's left foot around will allow you to move while balancing.